import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { Course } from './models/course';
import { DegreePlan } from './models/degree-plan';
import { Term } from './models/term';

@Injectable()
export class DataService {
	private plannerApiUrl: string;
	private searchApiUrl: string;

	constructor(private http: HttpClient, private configService: ConfigService) {
		this.plannerApiUrl = this.configService.apiPlannerUrl;
		this.searchApiUrl = this.configService.apiSearchUrl;
	}

	getDegreePlans(): Observable<DegreePlan[]> {
		return this.http.get<DegreePlan[]>(this.plannerApiUrl + '/degreePlan')
			.pipe(catchError(this.errorHandler));
	}

	getDegreePlannerCourseData(): Observable<Course[]> {
		return this.http.get<Course[]>(this.plannerApiUrl + '/degreePlan/520224/termcourses')
			.pipe(catchError(this.errorHandler));
	}

	getTerms(): Observable<Term[]> {
		return this.http.get<Term[]>(this.searchApiUrl + '/terms')
			.pipe(catchError(this.errorHandler));
	}

	private errorHandler(error: Response) {
		return throwError(error || 'Server Error');
	}
}
