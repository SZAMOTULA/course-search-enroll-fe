import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { NavigationComponent } from './navigation/navigation.component';
import { ConfigService } from './config.service';
import { DataService } from './data.service';
// import { ProfileService } from './profile.service'
import { throwIfAlreadyLoaded } from './module-import-check';

@NgModule({
	imports: [
		CommonModule, // we use *ngFor
		RouterModule, // we use router-outlet and routerLink
		SharedModule
	],
	exports: [
		NavigationComponent
	],
	declarations: [
		NavigationComponent
	],
	providers: [
		ConfigService,
		DataService
	],
})

export class CoreModule {
	constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
		throwIfAlreadyLoaded(parentModule, 'CoreModule');
	}
}
