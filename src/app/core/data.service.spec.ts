import { TestBed, inject } from '@angular/core/testing';
import {
	HttpClient,
	HttpBackend,
	HttpClientModule
} from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigService } from './config.service';
import { DataService } from './data.service';

describe('DataService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientModule],
			providers: [
				ConfigService,
				DataService,
				{ provide: HttpBackend, useClass: HttpClientTestingModule }
			]
		});
	});

	it('should create', inject([DataService], (service: DataService) => {
		expect(service).toBeTruthy();
	}));

});
