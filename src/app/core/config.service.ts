import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
	apiPlannerUrl = '/api/planner/v1/';
	apiSearchUrl = '/api/search/v1/';
	constructor() { }

}
