import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'academicYearState'
})

export class AcademicYearStatePipe implements PipeTransform {

	transform(academicYear: string, args?: any): any {
		const fullAcademicYear = academicYear.replace('-', ' - 20');
		const startYear = parseInt(fullAcademicYear.substring(0, 4), 10);
		const currentYear = (new Date()).getFullYear();

		if (startYear === currentYear) {
			return `Current: ${fullAcademicYear}`;
		} else if (startYear > currentYear) {
			return `Future: ${fullAcademicYear}`;
		} else if (startYear < currentYear) {
			return `Past: ${fullAcademicYear}`;
		}
		return fullAcademicYear;
	}

}
