import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'getTermDescription'
})

export class GetTermDescriptionPipe implements PipeTransform {

	transform(termCode: string, args?: any): any {
		let termDescription = '';
		const splitId = termCode.split('');
		const century = (19 + Number(splitId[0])).toString();
		const year = Number(splitId.splice(1, 2).join(''));
		const term = Number(splitId[1]);

		switch (term) {
			case 2:
				termDescription = 'Fall ' + century;
				termDescription += (year - 1).toString();
				break;
			case 3:
				termDescription = 'Fall ' + century;
				termDescription += (year - 1).toString();
				break;
			case 4:
				termDescription = 'Spring ' + century;
				termDescription += (year).toString();
				break;
			case 6:
				termDescription = 'Summer ' + century;
				termDescription += (year).toString();
				break;
		}
		return termDescription;
	}

}
