import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
	MatButtonModule,
	MatMenuModule,
	MatIconModule,
	MatTabsModule,
	MatExpansionModule,
	MatCardModule,
	MatSelectModule,
	MatSidenavModule } from '@angular/material';

import { GetTermDescriptionPipe } from './get-term-description.pipe';
import { AcademicYearStatePipe } from './academic-year-state.pipe';

const modules = [
	CommonModule,
	FormsModule,
	ReactiveFormsModule,
	MatButtonModule,
	MatMenuModule,
	MatIconModule,
	MatTabsModule,
	MatExpansionModule,
	MatCardModule,
	MatSelectModule,
	FlexLayoutModule,
	MatSidenavModule
];

@NgModule({
	imports: [ modules ],
	exports: [ modules, GetTermDescriptionPipe, AcademicYearStatePipe ],
	declarations: [ GetTermDescriptionPipe, AcademicYearStatePipe ]
})
export class SharedModule { }
