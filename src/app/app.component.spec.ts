import { TestBed, async } from '@angular/core/testing';
import { Component, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';

const routes: Routes = [
	{
		path: '',
		loadChildren: './degree-planner/degree-planner.module#DegreePlannerModule'
	}
];

@Component({
	template: '<div>lazy-loaded</div>'
	})
	class LazyComponent { }

	@NgModule({
	imports: [RouterModule],
	declarations: [LazyComponent]
	})
	class LazyModule { }

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [RouterModule, RouterTestingModule.withRoutes(routes)],
			schemas: [ NO_ERRORS_SCHEMA ],
			declarations: [
				AppComponent
			],
		}).compileComponents();
	}));
	it('should create the app', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));

	it('should render title in a router-outlet tag', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('router-outlet').textContent).toBeDefined();
		}));
});
