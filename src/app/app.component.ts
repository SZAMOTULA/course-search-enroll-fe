import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'app';
}

document.addEventListener('WebComponentsReady', function() {
	const customEvent = new CustomEvent('myuw-login', {
		detail: {
			person: {
				'firstName': 'Bucky'
			}
		}
	});
	document.dispatchEvent(customEvent);
});
