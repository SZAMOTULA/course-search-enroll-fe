import { Component } from '@angular/core';

import { DataService } from '../core/data.service';
import { DegreePlan } from '../core/models/degree-plan';
import { Course } from '../core/models/course';
import { Term } from '../core/models/term';

@Component({
	selector: 'app-degree-planner',
	templateUrl: './degree-planner.component.html',
	styleUrls: ['./degree-planner.component.scss']
})

export class DegreePlannerComponent {
	degreePlans: DegreePlan[];
	degreePlanCourses: Course[];
	selectedDegreePlan: number;
	terms: Term[];
	termsByAcademicYear: Object;

	constructor(private dataService: DataService) {
		this.dataService.getDegreePlans()
		.subscribe(data => {
			this.degreePlans = data;
			this.selectedDegreePlan = this.degreePlans[0].roadmapId;
		});

		this.dataService.getDegreePlannerCourseData()
		.subscribe(degreePlanCourses => {
			this.degreePlanCourses = degreePlanCourses;
		});

		this.dataService.getTerms()
		.subscribe(terms => {
			this.terms = terms;
			this.termsByAcademicYear = {};
			this.terms.forEach(term => {
				if (this.termsByAcademicYear[term.academicYear]) {
					this.termsByAcademicYear[term.academicYear].push(term);
				} else {
					this.termsByAcademicYear[term.academicYear] = [term];
				}
			});
		});
	}
}
