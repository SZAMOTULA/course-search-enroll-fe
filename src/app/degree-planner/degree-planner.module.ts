import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DegreePlannerComponent } from './degree-planner.component';
import { SharedModule } from '../shared/shared.module';
import { DegreePlannerRoutingModule } from './degree-planner.routing.module';
import { TermContainerComponent } from './term-container/term-container.component';
import { SidenavMenuItemComponent } from './sidenav-menu-item/sidenav-menu-item.component';
import { FavoritesContainerComponent } from './favorites-container/favorites-container.component';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		DegreePlannerRoutingModule
	],
	declarations: [
		DegreePlannerComponent,
		TermContainerComponent,
		SidenavMenuItemComponent,
		FavoritesContainerComponent
	]
})
export class DegreePlannerModule { }
