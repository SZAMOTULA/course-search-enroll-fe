import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-sidenav-menu-item',
	templateUrl: './sidenav-menu-item.component.html',
	styleUrls: ['./sidenav-menu-item.component.scss']
})
export class SidenavMenuItemComponent implements OnInit {
	events: string[] = [];
	opened: boolean;

	constructor() { }

	ngOnInit() {
	}
}
