import { Component, Input, OnInit } from '@angular/core';
import { Term } from '../../core/models/term';

@Component({
	selector: 'app-term-container',
	templateUrl: './term-container.component.html',
	styleUrls: ['./term-container.component.scss']
})

export class TermContainerComponent implements OnInit {
	@Input() terms: Term[];

	constructor() { }

	ngOnInit() {	}
}
