import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DegreePlannerComponent } from './degree-planner.component';


const routes: Routes = [
	{ path: '', component: DegreePlannerComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DegreePlannerRoutingModule { }
