import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { DegreePlannerModule } from './degree-planner/degree-planner.module';

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		DegreePlannerModule,
		CoreModule,
		SharedModule,
		AppRoutingModule,
		HttpClientModule
	],
	declarations: [
		AppComponent
	],
	providers: [],
	bootstrap: [ AppComponent ],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
